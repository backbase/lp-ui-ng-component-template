/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase B.V.
 *  ----------------------------------------------------------------
 *  Filename : index.spec.js
 *  Description: ${component.name} test Spec
 *  ----------------------------------------------------------------
 */

'use strict';

// include the component
var main = require('../../scripts/main');

require('angular-mocks');

var ngModule = window.module;
var ngInject = window.inject;

/*----------------------------------------------------------------*/
/* Module testing
/*----------------------------------------------------------------*/
describe('${component.name} testing suite', function() {
    it('should export an object', function() {
        expect(main).toBeObject();
    });
});

/*----------------------------------------------------------------*/
/* Directives testing
/*----------------------------------------------------------------*/
xdescribe('Directives ', function() {
    var scope, elem;
    // Load the component module, which contains the directive
    beforeEach(ngModule(main.name));


});
