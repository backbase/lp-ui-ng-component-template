define(function(require, exports, module) {

    'use strict';

    /**
     * ${component.name} custom element directive
     * @ngInject
     */
    exports.${component.transforms.name.camelCase} = function() {

        /**
         * Main directive link function
         * @param  {object} scope   Angular scope object
         * @param  {HTMLElement} elem    DOM element
         * @param  {object} attrs   Elem attributes
         * @param  {object} ngModel ngModelController
         * @return {undefined}
         */
        function linkFn(scope, elem, attrs, ngModel) {

        }

        /**
         * Compile function manipulate the DOM before the linking
         * @param  {HTMLElement} elem  DOM element
         * @param  {object} attrs Element attributes
         * @return {function}       Linked function
         */
        function compileFn(elem, attrs) {
            return linkFn;
        }
        /**
         * Inline template definition
         * @return {string} Template string
         */
        function templateFn() {
            return [
                '<div>',
                    // custom template
                '</div>'
            ].join('');
        }

        return {
            scope: {
                options: '=?',
                model: '=?ngModel',
                onEvent: '&'
            },
            template: templateFn,
            compile: compileFn,
            require: '?ngModel',
            // transclude: true,
            restrict: 'E',
            link: linkFn
        };
    };
});
