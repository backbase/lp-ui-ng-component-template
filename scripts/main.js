/**
 *  ----------------------------------------------------------------
 *  Copyright © Backbase Launchpad B.V.
 *  ----------------------------------------------------------------
 *  Filename : main.js
 *  Description: Main entry point ${component.name} component
 *  ----------------------------------------------------------------
 */
define( function (require, exports, module) {
    'use strict';

    module.name = '${component.name}';
    var base = require('base');
    var deps = [

    ];


    module.exports = base.createModule(module.name, deps)
        .directive(require('./directives'));
});
